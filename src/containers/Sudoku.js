import React, { useState, useEffect } from "react";
import "./Sudoku.css";
const TOTAL_CELLS = 91;
const SHOW_CELLS_COUNT = 30;

export default () => {
  const [sudoku, setSudoku] = useState([["Generating sudoku..."]]);
  const [sudokuAnswer, setSudokuAnswer] = useState([["Nothing is loaded"]]);
  const [showAnswer, setShowAnswer] = useState(false);

  useEffect(() => {
    fetch("/sudoku")
      .then(res => res.json())
      .then(sudokuAnswer => {
        setSudokuAnswer(sudokuAnswer);
        setSudoku(createPuzzle(sudokuAnswer));
      });
  }, []);

  const createPuzzle = sudoku => {
    // leave only 20 random cells
    const showCell = Array(TOTAL_CELLS)
      .fill(0)
      .fill(1, 0, SHOW_CELLS_COUNT)
      .sort(() => Math.random() > 0.5);
    return sudoku.map(row => row.map(cell => showCell.pop() && cell));
  };

  const renderSudoku = sudoku => {
    return (
      <table className="Sudoku-table">
        <tbody>
          {sudoku.map((row, i) => (
            <tr className="Sudoku-row" key={i}>
              {row.map((cell, j) => (
                <td className="Sudoku-td" key={j}>
                  {cell || <input className="Sudoku-input" />}
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    );
  };

  return (
    <div>
      <h1>Sudoku</h1>
      {renderSudoku(sudoku)}
      <button
        className="Sudoku-button"
        onClick={() => setShowAnswer(!showAnswer)}
      >
        {showAnswer ? "Hide answer" : "Show Answer"}
      </button>
      {showAnswer && renderSudoku(sudokuAnswer)}
    </div>
  );
};
