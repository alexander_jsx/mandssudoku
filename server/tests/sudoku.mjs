import {
  getRandomState,
  getGoodState,
  getTestValue,
  testValue,
  generateSudoku,
} from "../sudoku/index.mjs";

it("gets random state", () => {
  const state = getRandomState();
  // console.log(state);
});

it("gets empty good state", () => {
  const state = getGoodState();
  state.forEach(row => {
    row.forEach(cell => {
      expect(cell).toEqual(0);
    });
  });
});

it("get test value", () => {
  const state = getRandomState();
  const value = getTestValue(0, 0, state);
  // console.log(state);
  // console.log(value);
});

it("test value in new state", () => {
  const randomState = getRandomState();
  const goodState = getGoodState();
  const value = getTestValue(0, 0, randomState);
  const isGoodValue = testValue(0, 0, value, goodState);
  // console.log(isGoodValue);
});

it("generates sudoku", () => {
  const sudoku = generateSudoku();
  // console.log(sudoku);
});
