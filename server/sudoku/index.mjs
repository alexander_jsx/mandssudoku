const totalSize = 9;
const squereSize = 3;
const lastIndex = totalSize - 1;
const arrSize = { length: totalSize };

const randomOptions = () =>
  [...Array(totalSize).keys()].map(x => ++x).sort(() => Math.random() > 0.5);

// generate state with all possible options in random order
export const getRandomState = () =>
  Array.from(arrSize, () => Array.from(arrSize, () => randomOptions()));

export const getGoodState = () =>
  Array.from(arrSize, () => Array.from(arrSize, () => 0));

// get random value to test from our options
// if no testValues -> rollback and get another test value before
export const getTestValue = (x, y, randomState) => randomState[x][y].pop();

export const testValue = (x, y, value, currentState) => {
  const inRow = currentState[x].find(cell => cell === value);
  if (inRow) return false; // already in the row x

  const inColumn = currentState.map(row => row[y]).find(cell => cell === value);
  if (inColumn) return false; // already in column y

  const xStart = Math.floor(x / squereSize) * squereSize; // squere x beginning
  const yStart = Math.floor(y / squereSize) * squereSize; // squere y beginning
  for (let i = xStart; i < xStart + squereSize; i++) {
    for (let j = yStart; j < yStart + squereSize; j++) {
      if (currentState[i][j] === value) return false; // got you!
    }
  }

  // after all those checks we found good value
  return true;
};

export const generateSudoku = () => {
  const randomState = getRandomState();
  const goodState = getGoodState();
  let x = 0;
  let y = 0;

  // while last element is not filled - this should be done in a safe place
  while (goodState[lastIndex][lastIndex] === 0) {
    let foundGoodValue = false;
    while (!foundGoodValue) {
      const value = getTestValue(x, y, randomState);
      if (!value) {
        // the tricky part
        // we run out of options
        // we want to reset our options for this cell
        randomState[x][y] = randomOptions();
        // and get back to choose something else
        if (y === 0) {
          y = lastIndex;
          x -= 1;
        } else {
          y -= 1;
        }
        // reset the cell
        goodState[x][y] = 0;
        break;
      }
      const isGoodValue = testValue(x, y, value, goodState);
      if (isGoodValue) {
        // good value found
        foundGoodValue = true;
        goodState[x][y] = value;
        if (y === lastIndex) {
          y = 0;
          x += 1;
        } else {
          y += 1;
        }
      }
    }
  }
  return goodState;
};
