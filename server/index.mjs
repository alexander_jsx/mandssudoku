import express from "express";
import bodyParser from "body-parser";

import { generateSudoku } from "./sudoku";

const app = express();
const port = process.env.PORT || 5000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/sudoku", (req, res) => {
  res.json(generateSudoku());
});

app.listen(port, () => console.log(`Listening on port ${port}`));
